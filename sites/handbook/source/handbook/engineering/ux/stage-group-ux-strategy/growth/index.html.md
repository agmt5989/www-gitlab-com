---
layout: handbook-page-toc
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}



This page has been deprecated and all content can be found at [How the Growth Section Works](/handbook/product/growth/).
